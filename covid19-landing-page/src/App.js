import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import AboutUs from './components/AboutUs/AboutUs';
import Contagion from './components/Contagion/Contagion';
import Symptomps from './components/Symptomps/Symptomps';
import WhatShouldToDo from './components/ShouldToDo/ShouldToDo';
import ReportSection from './components/Report/Report';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <AboutUs />
      <Contagion />
      <Symptomps />
      <WhatShouldToDo />
      <ReportSection />
      <Footer />
    </div>
  );
}

export default App;
