import React from 'react';
import {
  Row,
  Col,
  Typography
} from 'antd';
import 'antd/dist/antd.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";

const dataList = [
  {
    img: require('../../assest/airTransition.png'),
    title: 'Air Transmission',
    text: 'Objectively evolve tactical expertise before extensible initiatives. Efficiently simplify',
  },
  {
    img: require('../../assest/contact.png'),
    title: 'Human Contacts',
    text: 'Washing your hands is one of thesimplest ways you can protect',
  },
  {
    img: require('../../assest/containerObject.png'),
    title: 'Containted Objects',
    text: 'Use the tissue while sneezing,In this way, you can protect your droplets.',
  },
];

const Contagion = () => {
  
  return (
    <React.Fragment>
      <Row>
        <Col
          span={24}
        >
          <Typography.Title
            className='section_title'
          >
            Covid-19
          </Typography.Title>
        </Col>
        <Col
          span={24}
        >
          <Typography.Title
            className='section_sub_title'
          >
            Contagion
          </Typography.Title>
        </Col>
        <Col
          span={24}
        >
          <Typography.Paragraph
            className='section_text_content'
          >
            Corona viruses are a type of virus. There are many different kinds, and some cause disease. A newly identified type
          </Typography.Paragraph>
        </Col>
            {dataList.map(item => (
              <Col
                span={24}
                className='carousel_container'
              >
                <div
                  className='carousel_item'
                >
                  <img
                    className='image'
                    src={item.img} />
                  <Typography.Title
                    className='section_sub_title'
                  >
                    {item.title}
                  </Typography.Title>
                  <Typography.Paragraph
                    className='section_text_content'
                  >
                    {item.text}
                  </Typography.Paragraph>
                </div>
              </Col>
            ))}
      </Row>
    </React.Fragment>
  );
};

export default Contagion;