import React from 'react';
import {
  Typography,
  Row,
  Col,
} from 'antd';
import 'antd/dist/antd.css'

const Symptomps = () => {

  return (
    <React.Fragment>
      <Row>
        <Col
          span={24}
        >
          <Typography.Title
            className='section_title'
          >
            Covid-19
          </Typography.Title>
        </Col>
        <Col
          span={24}
        >
          <Typography.Title
            className='section_sub_title'
          >
            Symptomps
          </Typography.Title>
        </Col>
        <Col
          span={24}
        >
          <img src={require('../../assest/symptomps.png')} />
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Symptomps;