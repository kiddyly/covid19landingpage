import React, {useState} from 'react';
import {
  Button,
  Popover,
  Typography,
  Layout,
  Row,
  Col,
} from 'antd';
import 'antd/dist/antd.css';

const Header = () => {
  
  const [popoverVisible, setPopoverVisible] = useState(false);

  const popoverMenu = (
    <div
      className='popover_menu_container'
    >
      <a
        className='menu_item'
      >
        Contagion
      </a>
      <a
        className='menu_item'
      >
        Symptoms
      </a>
      <a
        className='menu_item'
      >
        Prevention
      </a>
    </div>
  );
  return (
    <React.Fragment>
      <Layout>
        <Layout.Header
          className="navigation"
        >
          <img
            className="header_logo"
            src={require('../../assest/logo.png')} alt='' />
        <Popover
          content={popoverMenu}
          trigger="click"
          visible={popoverVisible}
          placement='bottom'
          onVisibleChange={() => setPopoverVisible(!popoverVisible)}
        >
          <Button
            className="header_button_menu"
          >
            Overview
          </Button>
        </Popover>
        </Layout.Header>
        <Layout.Content
          className="section_container"
        >
          <Row>
            <Col
              span={12}
            >
              <Typography.Title
                className='section_title'
              >
                COVID-19 Alert
              </Typography.Title>
              <Typography.Title
                className='section_sub_title'
              >
                Stay at Home quarantine
                To stop Corona virus
              </Typography.Title>
              <Typography.Paragraph
                className='section_text_content'
              >
                There is no specific medicine to prevent or treat coronavirus disease (COVID-19). People may need supportive care to .
              </Typography.Paragraph>
            </Col>
            <Col
              span={12}
            >
              <img src={require('../../assest/headerImage.png')} />
              <Button
                className='hero_button'
              >
                Let Us Help
              </Button>
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    </React.Fragment>
  );
};

export default Header;