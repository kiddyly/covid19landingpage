import React from 'react';
import {
  Layout,
  Row,
  Col,
  Typography,
  Button,
} from 'antd';
import 'antd/dist/antd.css';

const WhatShouldToDo = () => {

  return (
    <React.Fragment>
      <Row>
        <Col
          span={24}
        >
          <Row>
            <Col
              span={12}
            >
              <Layout>
                <Layout.Header
                  className='navigation'
                >
                  <span
                    className='number_mask'
                  >
                    01
                  </span>
                  <Typography.Title
                    className='section_sub_title'
                  >
                    Wear Masks
                  </Typography.Title>
                </Layout.Header>
                <Layout.Content
                  className='section_container'
                >
                  Continually seize impactful vortals rather than future-proof supply chains. Uniquely exploit emerging niches via fully tested meta-services. Competently pursue standards compliant leadership skills vis-a-vis pandemic "outside the box" thinking. Objectively 
                </Layout.Content>
              </Layout>
            </Col>
            <Col
              span={12}
            >
              <img src={require('../../assest/maskgirl.png')} alt='' />
            </Col>
          </Row>
        </Col>
        <Col
          span={24}
        >
          <Row>
            <Col
              span={12}
            >
              <img src={require('../../assest/washgirl.png')} alt='' />
            </Col>
            <Col
              span={12}
            >
              <Layout>
                <Layout.Header
                  className='navigation'
                >
                <Typography.Title
                  className='section_sub_title'
                >
                    Wash Your Hands
                  </Typography.Title>
                  <span
                    className='number_mask'
                  >
                    02
                  </span>
                </Layout.Header>
                <Layout.Content
                  className='section_container'
                >
                  Continually seize impactful vortals rather than future-proof supply chains. Uniquely exploit emerging niches via fully tested meta-services. Competently pursue standards compliant leadership skills vis-a-vis pandemic "outside the box" thinking. Objectively Continually seize impactful vortals 
                </Layout.Content>
              </Layout>
            </Col>
          </Row>
        </Col>
        <Col
          span={24}
        >
          <Row>
            <Col
              span={12}
            >
              <Layout>
                <Layout.Header
                  className='navigation'
                >
                  <span
                    className='number_mask'
                  >
                    03
                  </span>
                  <Typography.Title
                    className='section_sub_title'
                  >
                    Use nose - rag
                  </Typography.Title>
                </Layout.Header>
                <Layout.Content
                  className='section_container'
                >
                  Continually seize impactful vortals rather than future-proof supply chains. Uniquely exploit emerging niches via fully tested meta-services. Competently pursue standards compliant leadership skills vis-a-vis pandemic "outside the box" thinking. Objectively 
                </Layout.Content>
              </Layout>
            </Col>
            <Col
              span={12}
            >
              <img src={require('../../assest/useragGirl.png')} alt='' />
            </Col>
          </Row>
        </Col>
        <Col
          span={24}
        >
          <Row>
            <Col
              span={12}
            >
              <img src={require('../../assest/avoidContact.png')} alt='' />
            </Col>
            <Col
              span={12}
            >
              <Layout>
                <Layout.Header
                  className='navigation'
                >
                <Typography.Title
                  className='section_sub_title'
                >
                    Avoid Contacts
                  </Typography.Title>
                  <span
                    className='number_mask'
                  >
                    04
                  </span>
                </Layout.Header>
                <Layout.Content
                  className='section_container'
                >
                  Continually seize impactful vortals rather than future-proof supply chains. Uniquely exploit emerging niches via fully tested meta-services. Competently pursue standards compliant leadership skills vis-a-vis pandemic "outside the box" thinking. Objectively Continually seize impactful vortals 
                </Layout.Content>
              </Layout>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default WhatShouldToDo;