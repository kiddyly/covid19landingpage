import React from 'react';
import {
  Button,
  Row,
  Col,
  Typography,
} from 'antd';
import 'antd/dist/antd.css';

const AboutUs = () => {

  return (
    <React.Fragment>
      <Row>
        <Col
          span={12}
        >
          <img src={require('../../assest/virus.png')} alt='' />
        </Col>
        <Col
          span={12}
        >
          <Typography.Title
            className='section_title'
          >
            What Is Covid-19 
          </Typography.Title>
          <Typography.Title
            className='section_sub_title'
          >
            Coronavirus  
          </Typography.Title>
          <Typography.Paragraph
            className='section_text_content'
          >
            Corona viruses are a type of virus. There are many different kinds, and some cause disease....
          </Typography.Paragraph>
          <Button
            className='about_us_section_button'
          >
            Learn more
          </Button>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default AboutUs;