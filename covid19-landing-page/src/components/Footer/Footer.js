import React from 'react';
import {
  Row,
  Col,
  Typography,
  Layout
} from 'antd';
import 'antd/dist/antd.css';

const Footer = () => {

  return (
    <React.Fragment>
      <Row>
        <Col
          span={8}
          className='tree_menu'
        >
          <Typography.Text
            className='tree_menu_item'
          >
            Overview 
          </Typography.Text>
          <Typography.Text
            className='tree_menu_item'
          >
            Symptomps
          </Typography.Text>
          <Typography.Text
            className='tree_menu_item'
          >
            Prevention 
          </Typography.Text>
          <Typography.Text
            className='tree_menu_item'
          >
            Treatment 
          </Typography.Text>
        </Col>
        <Col
          span={16}
        >
          <Layout>
            <Layout.Header
              className='navigation'
            >
              <img src={require('../../assest/logo.png')} alt='' />
            </Layout.Header>
            <Layout.Content
              className='section_container'
            >
              <Typography.Text>
                2020 @ All rights reserved by pixelspark.co
              </Typography.Text>
            </Layout.Content>
          </Layout>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Footer;