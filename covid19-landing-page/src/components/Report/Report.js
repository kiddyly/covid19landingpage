import React, { useEffect, useState} from 'react';
import {
  List, Row, Col, Typography,
} from 'antd';
import 'antd/dist/antd.css';
import { VectorMap } from "react-jvectormap";
import { getCaseCountryData } from '../../services/getCountryData';

const ReportSection = () => {

  const [virusData, setVirusData] = useState([]);
  const [mapData, setMapData] = useState({});

  useEffect(() => {
    getCaseCountryData()
    .then(
      response => {
        setVirusData(response.listData);
        setMapData(response.mapData);
      }
    ).catch(
      error => {
        console.log('has an error', error);
      }
    );
  }, []);

  return (
    <React.Fragment>
      <Row
        className='report_section_container'
      >
        <Col
          span={24}
        >
          <Typography.Title
            className='section_sub_title'
          >
            Live Report
          </Typography.Title>
          <VectorMap
            map={'world_mill'}
            backgroundColor='transparent'
            containerStyle={{
              height: "320px"
            }}
            regionStyle={{
              initial: {
                fill: "#e4e4e4",
                "fill-opacity": 0.9,
                stroke: "none",
                "stroke-width": 0,
                "stroke-opacity": 0
              },
              hover: {
                "fill-opacity": 0.8,
                cursor: "pointer",
              },
              selected: {
                fill: "#2938bc" //color for the clicked country
              },
              
            }}
            regionsSelectable={true}
            series={{
              regions: [
                {
                  values: mapData, //this is your data
                  scale: ["#167C51", "#F44A45"], //your color game's here
                  normalizeFunction: "polynomial"
                }
              ]
            }}
            onRegionTipShow={(e, el, code) => {
              console.log(code)
              console.log(mapData[code])
            }}
          />
        </Col>
        <Col
          span={24}
        >
        </Col>
        <Col
          span={24}
          className='report_data_container'
        >
          <List
            dataSource={virusData}
            pagination={{
              pageSize: 3,
            }}
            renderItem={item => (
              <List.Item
                className='list_item'
              >
                  <img
                    className='image_flag'
                    src={item.countryInfo.flag} alt='' />

                  <p
                    className='report_infor_text_country'
                  >
                    {item.country}
                  </p>
                  <p
                    className='report_infor_text'
                  >
                    {item.cases}
                  </p>
              </List.Item>
            )}
          />
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default ReportSection;