export const getCaseCountryData = async () => {
  const url = 'https://corona.lmao.ninja/countries';
  return fetch(url).then(
    response => response.json()
  ).then(response => {
    let countryData = {}
    var key;
    for (key in response) {
      const countryCode = response[key].countryInfo.iso2;
      const caseInfo = response[key].cases;
      countryData[countryCode] = caseInfo;
    }
    return {
      listData: response,
      mapData: countryData,
    }
  })
  .catch(error => {
    console.log('Has an error', error);
  });
};